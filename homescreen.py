import json
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, SlideTransition,ScreenManager
from kivy.uix.textinput import TextInput

Window.size=(900,600)

class Homescreen(Screen):
    def __init__(self,ctrl,**kwargs):
        super(Homescreen,self).__init__(**kwargs)
        self.ctrl=ctrl
        self.base_home=Screen(name="homescreen")
        self.basContainer=BoxLayout(orientation="vertical")
        self.boxbutton=BoxLayout(padding=40,spacing=25)
        self.addElement()
        self.Signupclass()
        self.Loginfunc()
        self.mapgame()
    #fonction pour crée le screen de page principale
    def addElement(self,*args):
        self.img=Image(source="download.png")
        self.basContainer.add_widget(self.img)
        self.btnSignUp=Button(text="Register",font_size=25,size_hint=(.4,.4))
        self.btnSignUp.bind(on_press=self.screensignUp)
        self.btnlogIn=Button(text="Log In",font_size=25,size_hint=(.4,.4))
        self.btnlogIn.bind(on_press=self.screenlogin)
        self.boxbutton.add_widget(self.btnSignUp)
        self.boxbutton.add_widget(self.btnlogIn)
        self.basContainer.add_widget(self.boxbutton)
        self.base_home.add_widget(self.basContainer)

    def screensignUp(self,*args):
        self.base_home.manager.transition=SlideTransition(direction="right")
        self.base_home.manager.current="signupscreen"
    def screenlogin(self,*args):
        self.lab.text =""
        self.base_home.manager.transition=SlideTransition(direction="left")
        self.base_home.manager.current="loginclass"
    def homescreen(self,*args):
        self.base_home.manager.transition = SlideTransition(direction="left")
        self.base_home.manager.current = "homescreen"
        self.nextbtn.disabled = True
        self.name_inp.text = ""
        self.firstname_inp.text = ""
        self.mail_inp.text = ""
        self.username_inp.text = ""
        self.password_inp.text = ""
        self.mail_inplog.text=""
        self.password_inplog.text=""
    def homescreenretourpos(self,*args):
        self.savepos(self.ctrl.key)
        self.imgmove.pos=(Window.size[0]/2,Window.size[1]/2)
        self.base_home.manager.transition = SlideTransition(direction="left")
        self.base_home.manager.current = "homescreen"
        self.nextbtn.disabled = True
    def mapscreen(self,*args):
        self.base_home.manager.transition=SlideTransition(direction="right")
        self.base_home.manager.current="mapscreen"
        self.loadposition(self.ctrl.key)
    def maphome(self,*args):
        self.imgmove.pos=(Window.size[0]/2,Window.size[1]/2)
        self.base_home.manager.transition = SlideTransition(direction="right")
        self.base_home.manager.current = "mapscreen"

    #fonction pour crée le screen de enregistrement d'utilisateur
    def Signupclass(self):
        self.home_sign=Screen(name="signupscreen")
        self.homebox=BoxLayout(orientation="vertical")
        self.gridpersoninfo=GridLayout(cols=2,rows=5,size_hint=(1,.5))
        self.gridbtn=GridLayout(cols=1,rows=1,size_hint=(1,.25),padding=15)
        self.gridnextprev=GridLayout(cols=2,rows=1,size_hint=(1,.25),spacing=30,padding=20)

        self.namelabel = Label(text="Nom", font_size=25, bold=True)
        self.gridpersoninfo.add_widget(self.namelabel)
        self.name_inp = TextInput(hint_text="Ecrire ton nom ici", font_size=25, multiline=False)

        self.gridpersoninfo.add_widget(self.name_inp)

        self.firsnamelabel = Label(text="Prenom", font_size=25, bold=True)
        self.gridpersoninfo.add_widget(self.firsnamelabel)

        self.firstname_inp = TextInput(hint_text="Ecrire ton prenom ici", font_size=25, multiline=False)
        self.gridpersoninfo.add_widget(self.firstname_inp)

        self.maillabel = Label(text="E-mail", font_size=25, bold=True)
        self.gridpersoninfo.add_widget(self.maillabel)

        self.mail_inp = TextInput(hint_text="Ecrire ton E-mail ici", font_size=25, multiline=False)
        self.gridpersoninfo.add_widget(self.mail_inp)

        self.username = Label(text="Nom d'utilisateur", font_size=25, bold=True)
        self.gridpersoninfo.add_widget(self.username)

        self.username_inp = TextInput(hint_text="Ecrire ton nom d'utilisateur ici", font_size=25, multiline=False)
        self.gridpersoninfo.add_widget(self.username_inp)

        self.passswordlabel = Label(text="Mot de pass", font_size=25, bold=True)
        self.gridpersoninfo.add_widget(self.passswordlabel)

        self.password_inp = TextInput(hint_text="Ecrire ton password ici", font_size=25, multiline=False,password=True)
        self.gridpersoninfo.add_widget(self.password_inp)
        self.homebox.add_widget(self.gridpersoninfo)

        self.btnsignin = Button(text="Save", font_size=35,  pos_hint={"center_x": .5})
        self.btnsignin.bind(on_press=self.ctrl.chargeuser)
        self.gridbtn.add_widget(self.btnsignin)
        self.homebox.add_widget(self.gridbtn)

        self.nextbtn=Button(text="commencer",font_size=28,size_hint=(.2,.2),disabled=True)
        self.nextbtn.bind(on_press=self.maphome)
        self.gridnextprev.add_widget(self.nextbtn)
        self.prevbtn=Button(text="precedent",font_size=28,size_hint=(.2,.2))
        self.prevbtn.bind(on_press=self.homescreen)
        self.gridnextprev.add_widget(self.prevbtn)
        self.homebox.add_widget(self.gridnextprev)
        self.home_sign.add_widget(self.homebox)
    # les layout utiliser dans la fonction lgin()
    def Loginfunc(self):
        self.home_login=Screen(name = "loginclass")
        self.homeboxlogin = BoxLayout(orientation="vertical")
        self.gridpersoninfologin = GridLayout(cols=2, rows=2)
        self.gridlabel= GridLayout(cols=1, rows=1, size_hint=(1, .65))
        self.commencerprecedent=GridLayout(cols=2,rows=1,padding=45,spacing=30)
        self.login()
    #fonction pour ajouter les element de screen pour connecter
    def login(self, *args):
        self.maillabellog = Label(text="E-mail", font_size=25, bold=True)
        self.gridpersoninfologin.add_widget(self.maillabellog)
        self.mail_inplog = TextInput(hint_text="Ecrire ton E-mail ou ton nom d'utilisateur",
                                     font_size=25, multiline=False)
        self.gridpersoninfologin.add_widget(self.mail_inplog)

        self.passswordlabellog = Label(text="Mot de pass", font_size=25, bold=True)
        self.gridpersoninfologin.add_widget(self.passswordlabellog)

        self.password_inplog= TextInput(hint_text="Ecrire ton mot de passe ici",
                                        font_size=25,multiline=False,password=True)
        self.gridpersoninfologin.add_widget(self.password_inplog)
        self.homeboxlogin.add_widget(self.gridpersoninfologin)

        self.lab = Label(text="", font_size=30, color="white", pos_hint={"center_x": .5})
        self.gridlabel.add_widget(self.lab)

        self.prec = Button(text="precedent", font_size=35, color="red")
        self.prec.bind(on_press=self.homescreen)
        self.commencerprecedent.add_widget(self.prec)

        self.log = Button(text="Log in", font_size=35, color="blue")
        self.log.bind(on_press=self.ctrl.identification)
        self.commencerprecedent.add_widget(self.log)

        self.homeboxlogin.add_widget(self.gridlabel)
        self.homeboxlogin.add_widget(self.commencerprecedent)
        self.home_login.add_widget(self.homeboxlogin)

    #fonction pour creé le screen de jeux
    def mapgame(self):
        self.screenmap=Screen(name="mapscreen")
        self.mapgame=FloatLayout()

        self.btnup=Button(text="up",font_size=20,size_hint=(.1,.1),pos_hint={"x":.8,"y":.1})
        self.btnup.bind(on_press=self.ctrl.moveup)
        self.mapgame.add_widget(self.btnup,index=0)

        self.btndown=Button(text="down",font_size=20,size_hint=(.1,.1),pos_hint={"x":.8,"y":0})
        self.btndown.bind(on_press=self.ctrl.movedown)
        self.mapgame.add_widget(self.btndown,index=0)

        self.btnright=Button(text="right",font_size=20,size_hint=(.1,.1),pos_hint={"x":.9,"y":0})
        self.btnright.bind(on_press=self.ctrl.moveright)
        self.mapgame.add_widget(self.btnright,index=0)

        self.btnleft=Button(text="left",font_size=20,size_hint=(.1,.1),pos_hint={"x":.7,"y":0})
        self.btnleft.bind(on_press=self.ctrl.moveleft)
        self.mapgame.add_widget(self.btnleft,index=0)

        self.btnretour=Button(text="Retour",font_size=20,size_hint=(.12,.12))
        self.btnretour.bind(on_press=self.homescreenretourpos)
        self.mapgame.add_widget(self.btnretour,index=0)

        self.imgmove=Image(source="symbole.png",size_hint=(None,None),allow_stretch=True,
                           keep_ratio=False,pos=(Window.size[0]/2,Window.size[1]/2))
        self.mapgame.add_widget(self.imgmove,index=5)

        self.screenmap.add_widget(self.mapgame)
    def savepos(self,k):
        with open("file.json","r") as f:
            data=json.load(f)

        data[k]["position"]=self.imgmove.pos
        with open("file.json","w") as f:
            json.dump(data,f,indent=2)

    def loadposition(self,k):
        with open("file.json", "r") as f:
            data=json.load(f)
        if data[k]["position"] !="":
            self.imgmove.pos = data[k]["position"]
        else:
            self.imgmove.pos =(Window.size[0]/2,Window.size[1]/2)

class Controller():
    def __init__(self):
        self.sc=ScreenManager()
        self.homescreen=Homescreen(self)
        self.sc.add_widget(self.homescreen.base_home)
        self.sc.add_widget(self.homescreen.home_sign)
        self.sc.add_widget(self.homescreen.home_login)
        self.sc.add_widget(self.homescreen.screenmap)
        self.liste_user=[]
        self.key=0
        #self.usernameuser=False

    def popfunction(self):
        self.box = BoxLayout(orientation="vertical")
        self.box.add_widget(Label(text="il faut remplir tous les champs", font_size=17))
        self.btnpop = Button(text="Ok", color="red", font_size=15, size_hint=(.35, .35), pos_hint={'center_x': 0.5})
        self.box.add_widget(self.btnpop)
        self.pop = Popup(title="Info", content=self.box, size_hint=(None, None),
                         size=(250, 200), auto_dismiss=False,
                         title_size=30, title_color="green", title_align="center")
        self.btnpop.bind(on_press=self.pop.dismiss)
        self.pop.open()

    def popfunctionusernameMail(self):
        self.box = BoxLayout(orientation="vertical")
        self.box.add_widget(Label(text="E-mail ou nom d'utilisateur est déja existé", font_size=17))
        self.btnpop = Button(text="Ok", color="red", font_size=15, size_hint=(.35, .35), pos_hint={'center_x': 0.5})
        self.box.add_widget(self.btnpop)
        self.pop = Popup(title="Info", content=self.box, size_hint=(None, None),
                         size=(350, 200), auto_dismiss=False,
                         title_size=30, title_color="green", title_align="center")
        self.btnpop.bind(on_press=self.pop.dismiss)
        self.pop.open()


    def identification(self, *args):
        with open("file.json", "r") as f:
            data = json.load(f)
        for i, j in data.items():
            if (self.homescreen.mail_inplog.text==j["E-mail"] or self.homescreen.mail_inplog.text ==j["username"])\
                    and self.homescreen.password_inplog.text==j["password"]:

                self.key=i

                self.homescreen.mail_inplog.text=""
                self.homescreen.password_inplog.text=""
                self.homescreen.mapscreen()
            else:
                self.homescreen.lab.text="your username (e-mail) or your password is wrong"

    def chargeuser(self,*args):
        self.usernameuser = False
        if self.homescreen.name_inp.text!="" and self.homescreen.firstname_inp.text!="" and\
        self.homescreen.mail_inp.text!=""and self.homescreen.username_inp.text!="" \
                and self.homescreen.password_inp.text!="":
            with open("file.json", "r") as f:
                data = json.load(f)
            for i, j in data.items():
                if (self.homescreen.mail_inp.text == j["E-mail"] or self.homescreen.username_inp.text == j["username"]):
                    self.usernameuser=True
            if self.usernameuser:
                self.popfunctionusernameMail()
            else:
                #else:
                self.cordonee = {
                    "name": self.homescreen.name_inp.text,
                    "firstname": self.homescreen.firstname_inp.text,
                    "E-mail": self.homescreen.mail_inp.text,
                    "username": self.homescreen.username_inp.text,
                    "password": self.homescreen.password_inp.text,
                    "position":""
                                }
                with open("file.json","r") as f:
                    donnee=json.load(f)
                self.key =str(len(donnee))
                donnee[len(donnee)]=self.cordonee

                with open("file.json","w") as f:
                    json.dump(donnee,f,indent=2)

                self.homescreen.name_inp.text=""
                self.homescreen.firstname_inp.text=""
                self.homescreen.mail_inp.text=""
                self.homescreen.username_inp.text=""
                self.homescreen.password_inp.text=""

                self.homescreen.nextbtn.disabled = False

        else:
            self.popfunction()
    # fonction pour bouger à droite
    def moveright(self,*args):
        x=Window.size[0]-self.homescreen.imgmove.size[0]
        if self.homescreen.imgmove.pos[0] >= x:
            self.homescreen.imgmove.pos[0] = 0
        else:
            self.homescreen.imgmove.pos[0] += 15

    # fonction pour bouger à gauche
    def moveleft(self,*args):
        x=Window.size[0]-self.homescreen.imgmove.size[0]
        if self.homescreen.imgmove.pos[0] <= 0:
            self.homescreen.imgmove.pos[0]=x
        else:
            self.homescreen.imgmove.pos[0]-=15

    # fonction pour bouger vers la haut
    def moveup(self,*args):
        x = Window.size[1] - self.homescreen.imgmove.size[1]
        if self.homescreen.imgmove.pos[1] >= x:
            self.homescreen.imgmove.pos[1] = 0
        else:
            self.homescreen.imgmove.pos[1] += 15

    # fonction pour bouger vers le droite
    def movedown(self,*args):
        x = Window.size[1] - self.homescreen.imgmove.size[1]
        if self.homescreen.imgmove.pos[1] <= 0:
            self.homescreen.imgmove.pos[1]=x
        else:
            self.homescreen.imgmove.pos[1]-=15






